<?php

/**
 * @file
 * localizedrupalorg_stories.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function localizedrupalorg_stories_taxonomy_default_vocabularies() {
  return array(
    'vocabulary_2' => array(
      'name' => 'Group tags',
      'machine_name' => 'vocabulary_2',
      'description' => 'Use to tag your posts with l10n-server-{langcode} type of tags and others of your choice.',
      'hierarchy' => 1,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
