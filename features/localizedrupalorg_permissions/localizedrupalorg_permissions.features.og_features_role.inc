<?php

/**
 * @file
 * localizedrupalorg_permissions.features.og_features_role.inc
 */

/**
 * Implements hook_og_features_default_roles().
 */
function localizedrupalorg_permissions_og_features_default_roles() {
  $roles = array();

  // Exported OG Role: 'node:l10n_group:translation community manager'.
  $roles['node:l10n_group:translation community manager'] = array(
    'gid' => 0,
    'group_type' => 'node',
    'group_bundle' => 'l10n_group',
    'name' => 'translation community manager',
  );

  // Exported OG Role: 'node:l10n_group:translation community moderator'.
  $roles['node:l10n_group:translation community moderator'] = array(
    'gid' => 0,
    'group_type' => 'node',
    'group_bundle' => 'l10n_group',
    'name' => 'translation community moderator',
  );

  // Exported OG Role: 'node:l10n_group:translation self-moderator'.
  $roles['node:l10n_group:translation self-moderator'] = array(
    'gid' => 0,
    'group_type' => 'node',
    'group_bundle' => 'l10n_group',
    'name' => 'translation self-moderator',
  );

  return $roles;
}
