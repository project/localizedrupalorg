<?php

/**
 * @file
 * localizedrupalorg_permissions.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function localizedrupalorg_permissions_user_default_roles() {
  $roles = array();

  // Exported role: administrator.
  $roles['administrator'] = array(
    'name' => 'administrator',
    'weight' => 0,
  );

  // Exported role: all teams manager.
  $roles['all teams manager'] = array(
    'name' => 'all teams manager',
    'weight' => 0,
  );

  // Exported role: community.
  $roles['community'] = array(
    'name' => 'community',
    'weight' => 0,
  );

  // Exported role: confirmed.
  $roles['confirmed'] = array(
    'name' => 'confirmed',
    'weight' => 0,
  );

  return $roles;
}
