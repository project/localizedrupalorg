<?php

/**
 * Implements hook_install().
 */
function localizedrupalorg_install() {
  //Install Drupal7 Updates
  localizedrupalorg_update_7101();
}

function localizedrupalorg_update_7101() {
  $filter_names = array(
    '2' => 'filtered_html',
    '4' => 'full_html',
    '8' => 'message_plain_text',
    '9' => 'plain_text'
  );

  foreach ($filter_names AS $existing => $new) {
    db_update('filter_format')
      ->fields(array('format' => $new))
      ->condition('format', $existing)
      ->execute();
    db_update('filter')
      ->fields(array('format' => $new))
      ->condition('format', $existing)
      ->execute();
    db_update('field_data_body')
      ->fields(array('body_format' => $new))
      ->condition('body_format', $existing)
      ->execute();
    db_update('field_revision_body')
      ->fields(array('body_format' => $new))
      ->condition('body_format', $existing)
      ->execute();
    db_update('field_data_comment_body')
      ->fields(array('comment_body_format' => $new))
      ->condition('comment_body_format', $existing)
      ->execute();
    db_update('field_revision_comment_body')
      ->fields(array('comment_body_format' => $new))
      ->condition('comment_body_format', $existing)
      ->execute();
    db_update('role_permission')
      ->fields(array('permission' => 'use text format ' . $new))
      ->condition('permission', 'use text format ' . $existing)
      ->execute();
    db_update('block_custom')
      ->fields(array('format' => $new))
      ->condition('format', $existing)
      ->execute();
    db_update('users')
      ->fields(array('signature_format' => $new))
      ->condition('signature_format', $existing)
      ->execute();
  }

  variable_set('filter_default_format', 'filtered_html');
  variable_set('filter_fallback_format', 'plain_text');

  return array();
}

function localizedrupalorg_update_7102() {
  require_once drupal_get_path('module', 'views_ui') . '/views_ui.module';

  $view = views_ui_cache_load('og_members_ldo');
  $view->delete();
  ctools_object_cache_clear('view', $view->name);
}

/*
 * Set a bunch of default variables for Drupal 7 localize.
 */
function localizedrupalorg_update_7103() {
  variable_set('og_7000_access_field_default_value', 0);
  variable_set('preprocess_css', 0);
  variable_set('preprocess_js', 0);
  variable_set('admin_theme', 'adminimal');
}

/*
 * Disable messaging text format for now.
 */
function localizedrupalorg_update_7104() {
  $format = filter_format_load('message_plain_text');
  filter_format_disable($format);
}

/**
 * Remove multiple copies of distributions.
 */
function localizedrupalorg_update_7105() {
  db_query("DELETE fm FROM {l10n_server_release} lsr INNER JOIN {l10n_packager_file} lpf ON lpf.rid = lsr.rid INNER JOIN {file_managed} fm ON fm.fid = lpf.fid WHERE lsr.download_link LIKE '%-core.tar.gz'");
  db_query("DELETE f FROM {l10n_server_release} lsr INNER JOIN {l10n_packager_file} lpf ON lpf.rid = lsr.rid INNER JOIN {files} f ON f.fid = lpf.fid WHERE lsr.download_link LIKE '%-core.tar.gz'");
  db_query("DELETE lpf FROM {l10n_server_release} lsr INNER JOIN {l10n_packager_file} lpf ON lpf.rid = lsr.rid WHERE lsr.download_link LIKE '%-core.tar.gz'");
  db_query("DELETE lpr FROM {l10n_server_release} lsr INNER JOIN {l10n_packager_release} lpr ON lpr.rid = lsr.rid WHERE lsr.download_link LIKE '%-core.tar.gz'");
  db_query("DELETE lse FROM {l10n_server_release} lsr INNER JOIN {l10n_server_error} lse ON lse.rid = lsr.rid WHERE lsr.download_link LIKE '%-core.tar.gz'");
  db_query("DELETE lsf FROM {l10n_server_release} lsr INNER JOIN {l10n_server_file} lsf ON lsf.rid = lsr.rid WHERE lsr.download_link LIKE '%-core.tar.gz'");
  foreach (array_chunk(db_query("SELECT rid FROM {l10n_server_release} lsr WHERE lsr.download_link LIKE '%-core.tar.gz'")->fetchCol(), 10) as $rids) {
    db_query("DELETE FROM {l10n_server_line} WHERE rid IN (:rids)", array(':rids' => $rids));
  }
  db_query("DELETE lsr FROM {l10n_server_release} lsr WHERE lsr.download_link LIKE '%-core.tar.gz'");
}

/**
 * Migrate upload.module data to the newly created file field.
 */
function localizedrupalorg_update_7106(&$sandbox) {
  // Remove existing stale packages
  //drush_log("starting update", true);
  if (!variable_get('ldo_7105_inprogress', false)) {
    //drush_log("deleting stale managed files", true);
    db_delete('file_managed')
      ->condition('uid', 0)
      ->execute();
    db_delete('file_usage')
      ->condition('module', 'l10n_packager')
      ->execute();
    variable_set('ldo_7105_inprogress', true);
  }

  $fid = variable_get('ldo_7105_fid', -1);
  if (!isset($sandbox['progress'])) {
    //drush_log("deleting stale files from l10n_packager_file", true);
    // Delete stale rows from {upload} where the fid is not in the {files} table.
    db_delete('l10n_packager_file')
      ->notExists(
        db_select('files', 'f')
        ->fields('f', array('fid'))
        ->where('f.fid = {l10n_packager_file}.fid')
      )
      ->execute();
    // Initialize batch update information.
    $sandbox['progress'] = variable_get('ldo_7105_progress', 0);
    $sandbox['last_fid_processed'] = $fid;
    $sandbox['max'] = db_query("SELECT COUNT(*) FROM {l10n_packager_file}")->fetchField();
  }

  $basename = variable_get('file_directory_path', conf_path() . '/files');
  $scheme = file_default_scheme() . '://';

  // Determine vids for this batch.
  // Process all files attached to a given revision during the same batch.
  $limit = variable_get('ldo_l10n_update_batch_size', 60000);
  $count = 0;

  $query = db_select('files', 'fi');
  $query->join('l10n_packager_file', 'pf', 'fi.fid = pf.fid');
  $query->fields('fi', array('fid', 'uid', 'filename', 'filepath', 'filemime', 'filesize', 'status', 'timestamp'))
    ->fields('pf', array('drid'))
    ->condition('fi.fid', $sandbox['last_fid_processed'], '>')
    ->orderBy('fi.fid', 'ASC')
    ->range($count, $limit);
  $result = $query->execute();

  //drush_log("Finished grabbing files", true);
  while($file = $result->fetchAssoc()) {
    // We will convert filepaths to URI using the default scheme
    // and stripping off the existing file directory path.
    $file['uri'] = $scheme . preg_replace('!^' . preg_quote($basename) . '!', '', $file['filepath']);
    // Normalize the URI but don't call file_stream_wrapper_uri_normalize()
    // directly, since that is a higher-level API function which invokes
    // hooks while validating the scheme, and those will not work during
    // the upgrade. Instead, use a simpler version that just assumes the
    // scheme from above is already valid.
    if (($file_uri_scheme = file_uri_scheme($file['uri'])) && ($file_uri_target = file_uri_target($file['uri']))) {
      $file['uri'] = $file_uri_scheme . '://' . $file_uri_target;
    }
    unset($file['filepath']);
    // Insert into the file_managed table.
    // Each fid and uri should only be stored once in file_managed.
    //drush_log("selecting file with " . $file['fid']);
    try {
      db_insert('file_managed')
        ->fields(array(
          'fid' => $file['fid'],
          'uid' => $file['uid'],
          'filename' => $file['filename'],
          'uri' => $file['uri'],
          'filemime' => $file['filemime'],
          'filesize' => $file['filesize'],
          'status' => $file['status'],
          'timestamp' => $file['timestamp'],
        ))
        ->execute();

        // Add the usage entry for the file.
        $file = (object) $file;
        file_usage_add($file, 'l10n_packager', 'l10n_package', $file->drid);
    } catch (PDOException $e) {
      //drush_log("Skipping fid or uri that already exists: " . $e->getMessage());
      $file = (object) $file;
    }
    $fid = $file->fid;
    $count++;
  }

  $sandbox['last_fid_processed'] = $fid;
  variable_set('ldo_7105_fid', $fid);

  // If less than limit node revisions were processed, the update process is
  // finished.
  if ($count < $limit) {
    $finished = TRUE;
  }
  // Update our progress information for the batch update.
  $sandbox['progress'] += $count;
  variable_set('ldo_7105_progress', $sandbox['progress']);

  // If there's no max value then there's nothing to update and we're finished.
  if (empty($sandbox['max']) || isset($finished)) {
    variable_del('ldo_7105_inprogress');
    variable_del('ldo_7105_progress');
    variable_del('ldo_7105_fid');

    // Remove stale file duplicates from the l10n_packager_file table
    db_delete('l10n_packager_file')
      ->notExists(
        db_select('file_managed', 'f')
        ->fields('f', array('fid'))
        ->where('f.fid = {l10n_packager_file}.fid')
      )
      ->execute();
    return t('l10n files have been successfully migrated');
  }
  else {
    // Indicate our current progress to the batch update system.
    $sandbox['#finished'] = $sandbox['progress'] / $sandbox['max'];
    drush_log("Processed " . $sandbox['progress'] . " of " . $sandbox['max'] . "(" . $sandbox['#finished'] . "%)");
  }
}

/**
 * Set the killswitch for l10n_packager_update_7000().
 */
function localizedrupalorg_update_7107() {
  variable_set('l10n_packager_skip_update_7000', TRUE);
}
