<?php

/**
 * Implements hook_drush_command().
 */
function localizedrupalorg_drush_command() {

  $items['localizedrupalorg-revert-features'] = array(
    'description' => 'Revert LDO related features.',
    'aliases' => array('ldo-fr'),
  );

  $items['localizedrupalorg-remove-passive-users'] = array(
    'description' => 'Convert passive users into OG_BLOCKED users, remove team member role.',
    'aliases' => array('ldo-ogb'),
  );

  $items['localizedrupalorg-migrate-created-date'] = array(
    'description' => 'Update created date for group owners to d6 settings.',
    'aliases' => array('ldo-mcd'),
  );

  $items['localizedrupalorg-remove-adminimal-blocks'] = array(
    'description' => 'Remove erronious adminimal blocks from display.',
    'aliases' => array('ldo-abr'),
  );
  return $items;
}

/*
 * Revert LDO features.
 */
function drush_localizedrupalorg_revert_features() {
  $revert = array(
    'localizedrupalorg_groups' => array('field_instance'),
    'localizedrupalorg_permissions' => array('og_features_permission', 'og_features_role', 'user_permission'),
    'localizedrupalorg_polls' => array('field_instance'),
    'localizedrupalorg_stories' => array('field_instance'),
    'localizedrupalorg_users' => array('field_instance'),
    'localizedrupalorg_wikis' => array('field_instance'),
  );
  features_revert($revert);
}

function drush_localizedrupalorg_remove_adminimal_blocks() {
  db_update('block')
    ->fields(array('status' => 0, 'region' => '-1'))
    ->condition('theme', 'adminimal')
    ->condition('region', 'content_before')
    ->execute();
}

function drush_localizedrupalorg_remove_passive_users() {
  $passive_users = db_select('og_users_roles', 'ogur')
    ->fields('ogur', array('uid', 'gid'))
    ->condition('rid', 19)
    ->condition('group_type', 'node')
    ->execute();

  // Iterate through blocked users, remove their passive user role and make them blocked.
  while($record = $passive_users->fetchAssoc()) {
    og_role_revoke('node', $record['gid'], $record['uid'], 19);
    if ($og_membership = og_get_membership('node', $record['gid'], 'user', $record['uid'])) {
      $og_membership->state = OG_STATE_BLOCKED;
      $og_membership->save();
    }
  }

  // After all passive users are migrated, delete the passive users role.
  og_role_delete(19);
  // Remove the team member role
  og_role_delete(8);
    // Remove the Administrator default role member
  og_role_delete(3);

  // Remove the Global roles that were already imported into OG
  // Translation team member.
  user_role_delete(8);
  // Passive team member.
  user_role_delete(19);
  // Translation community manager.
  user_role_delete(14);
  // Translation self-moderator.
  user_role_delete(10);
  // Translation community moderator.
  user_role_delete(12);

}

function drush_localizedrupalorg_migrate_created_date() {
  //get the unixtime from the migration, make it valid from 2 hours before
  $datetime = strtotime('-3 week');
  $uids = db_select('og_membership', 'ogm')
    ->fields('ogm', array('etid', 'gid', 'id'))
    ->condition('created', $datetime, '>')
    ->execute();

  while ($record = $uids->fetchAssoc()) {
    $created = db_select('d6_og_uid', 'oguid')
      ->fields('oguid', array('created'))
      ->condition('uid', $record['etid'])
      ->condition('nid', $record['gid'])
      ->execute()
      ->fetchField();
    if ($created) {
      db_update('og_membership')
        ->fields(array('created' => $created))
        ->condition('id', $record['id'])
        ->execute();
    }
  }
}