<?php

/**
 * @file
 * localizedrupalorg_permissions.features.og_features_permission.inc
 */

/**
 * Implements hook_og_features_default_permissions().
 */
function localizedrupalorg_permissions_og_features_default_permissions() {
  $permissions = array();

  // Exported og permission: 'node:l10n_group:add user'
  $permissions['node:l10n_group:add user'] = array(
    'roles' => array(
      'translation community manager' => 'translation community manager',
    ),
  );

  // Exported og permission: 'node:l10n_group:administer group'
  $permissions['node:l10n_group:administer group'] = array(
    'roles' => array(
      'translation community manager' => 'translation community manager',
    ),
  );

  // Exported og permission: 'node:l10n_group:approve and deny subscription'
  $permissions['node:l10n_group:approve and deny subscription'] = array(
    'roles' => array(
      'translation community manager' => 'translation community manager',
    ),
  );

  // Exported og permission: 'node:l10n_group:browse translations'
  $permissions['node:l10n_group:browse translations'] = array(
    'roles' => array(
      'member' => 'member',
      'non-member' => 'non-member',
    ),
  );

  // Exported og permission: 'node:l10n_group:create story content'
  $permissions['node:l10n_group:create story content'] = array(
    'roles' => array(
      'member' => 'member',
      'translation community manager' => 'translation community manager',
    ),
  );

  // Exported og permission: 'node:l10n_group:create wiki content'
  $permissions['node:l10n_group:create wiki content'] = array(
    'roles' => array(
      'member' => 'member',
      'translation community manager' => 'translation community manager',
    ),
  );

  // Exported og permission: 'node:l10n_group:delete any story content'
  $permissions['node:l10n_group:delete any story content'] = array(
    'roles' => array(
      'translation community manager' => 'translation community manager',
    ),
  );

  // Exported og permission: 'node:l10n_group:delete any wiki content'
  $permissions['node:l10n_group:delete any wiki content'] = array(
    'roles' => array(
      'translation community manager' => 'translation community manager',
    ),
  );

  // Exported og permission: 'node:l10n_group:delete own story content'
  $permissions['node:l10n_group:delete own story content'] = array(
    'roles' => array(
      'translation community manager' => 'translation community manager',
    ),
  );

  // Exported og permission: 'node:l10n_group:delete own wiki content'
  $permissions['node:l10n_group:delete own wiki content'] = array(
    'roles' => array(
      'translation community manager' => 'translation community manager',
    ),
  );

  // Exported og permission: 'node:l10n_group:export gettext templates and translations'
  $permissions['node:l10n_group:export gettext templates and translations'] = array(
    'roles' => array(
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:l10n_group:import gettext files'
  $permissions['node:l10n_group:import gettext files'] = array(
    'roles' => array(
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:l10n_group:manage members'
  $permissions['node:l10n_group:manage members'] = array(
    'roles' => array(
      'translation community manager' => 'translation community manager',
    ),
  );

  // Exported og permission: 'node:l10n_group:manage permissions'
  $permissions['node:l10n_group:manage permissions'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:l10n_group:manage roles'
  $permissions['node:l10n_group:manage roles'] = array(
    'roles' => array(
      'translation community manager' => 'translation community manager',
    ),
  );

  // Exported og permission: 'node:l10n_group:moderate own suggestions'
  $permissions['node:l10n_group:moderate own suggestions'] = array(
    'roles' => array(
      'translation self-moderator' => 'translation self-moderator',
    ),
  );

  // Exported og permission: 'node:l10n_group:moderate suggestions from others'
  $permissions['node:l10n_group:moderate suggestions from others'] = array(
    'roles' => array(
      'translation community moderator' => 'translation community moderator',
    ),
  );

  // Exported og permission: 'node:l10n_group:start over packages'
  $permissions['node:l10n_group:start over packages'] = array(
    'roles' => array(
      'translation community manager' => 'translation community manager',
    ),
  );

  // Exported og permission: 'node:l10n_group:submit suggestions'
  $permissions['node:l10n_group:submit suggestions'] = array(
    'roles' => array(
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:l10n_group:subscribe'
  $permissions['node:l10n_group:subscribe'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:l10n_group:subscribe without approval'
  $permissions['node:l10n_group:subscribe without approval'] = array(
    'roles' => array(
      'non-member' => 'non-member',
    ),
  );

  // Exported og permission: 'node:l10n_group:unsubscribe'
  $permissions['node:l10n_group:unsubscribe'] = array(
    'roles' => array(
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:l10n_group:update any story content'
  $permissions['node:l10n_group:update any story content'] = array(
    'roles' => array(
      'translation community manager' => 'translation community manager',
    ),
  );

  // Exported og permission: 'node:l10n_group:update any wiki content'
  $permissions['node:l10n_group:update any wiki content'] = array(
    'roles' => array(
      'member' => 'member',
      'translation community manager' => 'translation community manager',
    ),
  );

  // Exported og permission: 'node:l10n_group:update group'
  $permissions['node:l10n_group:update group'] = array(
    'roles' => array(
      'translation community manager' => 'translation community manager',
    ),
  );

  // Exported og permission: 'node:l10n_group:update own story content'
  $permissions['node:l10n_group:update own story content'] = array(
    'roles' => array(
      'member' => 'member',
      'translation community manager' => 'translation community manager',
    ),
  );

  // Exported og permission: 'node:l10n_group:update own wiki content'
  $permissions['node:l10n_group:update own wiki content'] = array(
    'roles' => array(
      'member' => 'member',
      'translation community manager' => 'translation community manager',
    ),
  );

  return $permissions;
}
